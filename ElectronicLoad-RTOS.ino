#include "nilconf.h"
#include <NilRTOS.h>
#include <NilTimer1.h>
//#include <NilAnalog.h>
// NilSerial will be the hardware UART on pins 0/1
#include "NilSerial32u4.h"

//#include <avr_heap.h>

//#include <EEPROM.h>
#include <avr/eeprom.h>

// WDT doesn't play nicely with the USB code
//#include <avr/wdt.h>

#include <ClickEncoder.h>
#include <LiquidCrystal.h>
#include <SPI.h>

// Use nilAnalog variant
//#define DEFAULT 0
//#define INTERNAL 3
//#define analogReference nilAnalogReference
//#define analogRead nilAnalogRead

// Pins:
const char pin_vSense = A2;
const char pin_rotary_a = 3;
const char pin_rotary_b = 2;
const char pin_rotary_e = A3;
const char pin_dac_fs = A1;
const char pin_dac_ss = A0;

LiquidCrystal lcd(4, 5, 6, 9, 7, 8);

// Modes:
//  OFF - Have no load, put the DAC into shutdown
//  CC - Constant current
//  CP - Constant power, CPU regulated
typedef enum { MODE_OFF, MODE_CC, MODE_CP } modeTypes;
volatile modeTypes currentMode = MODE_OFF;

// Display modes
typedef enum { DISP_STANDARD, DISP_POWER } displayModes;
volatile displayModes displayMode = DISP_STANDARD;

#define CALIBRATION_VARIANT ((uint16_t)0xBA04)

struct {
  uint16_t calibrationVariant;
  uint16_t vSenseOffset;
  float vSenseLow;
  float vSenseMax;
  int16_t currentOffset;
  float temperatureOffset;
  float temperatureGain;
  float iSenseLow;
  float iSenseMax;
  float polyC;
  float polyB;
  float polyA;
} calibration;

// poly C,B,A are used as Y = Cx^2 + Bx + A

// Target settings
SEMAPHORE_DECL(currentCurrentLock,1);
volatile uint16_t currentCurrent = 0;
SEMAPHORE_DECL(targetLock,1);
volatile uint16_t targetCurrent = 0;
volatile uint32_t targetPower = 0;
volatile uint16_t targetPowerCurrent = 0;

// Voltage and power sensing
float vSenseLoadTmp;
float iSenseLoadTmp;
float vSensePowerTmp;
float vSenseADC = 0.0; // Raw ADC value
float iSenseADC = 0.0;
SEMAPHORE_DECL(vSenseLoadLock,1);
volatile uint16_t vSenseLoad = 0;
volatile uint16_t iSenseLoad = 0;
volatile uint16_t vSensePower = 0;
volatile uint16_t vcc = 0;

// Encoder information
SEMAPHORE_DECL(encoderLock, 1);
SEMAPHORE_DECL(encoderChange, 0);
SEMAPHORE_DECL(encoderButtonChange, 0);
uint16_t encoderValue = 0;
ClickEncoder::Button encoderButton;
ClickEncoder *encoder = NULL;

// System "clocks"
SEMAPHORE_DECL(monitoringTick, 1);

// Run control for constant power thread
SEMAPHORE_DECL(modeChangedToCP, 0);

// ADC Interrupt
SEMAPHORE_DECL(adcSem, 0);
NIL_IRQ_HANDLER(ADC_vect) {
  NIL_IRQ_PROLOGUE();
  nilSysLockFromISR();
  nilSemSignalI(&adcSem);
  nilSysUnlockFromISR();
  NIL_IRQ_EPILOGUE();
}

NIL_WORKING_AREA(waRotaryEncoderThread, 96);
NIL_THREAD(rotaryEncoderThread, arg) {
  uint16_t last = 0;
  uint16_t tick = 0;
  nilTimer1Start(1000);
  while(1) {
    nilTimer1Wait();
    if(++tick == 50) {
      nilSemSignal(&monitoringTick);
      tick = 0;
    }
    encoder->service();
    uint16_t next = encoder->getValue();
    nilSemWait(&encoderLock);
    if(encoderValue + next >= 0 && encoderValue + next < 4096)
      encoderValue += next;
    if(encoderValue != last) {
      last = encoderValue;
      nilSemSignal(&encoderChange);
    }
    ClickEncoder::Button b = encoder->getButton();
    if(b != ClickEncoder::Open) {
      encoderButton = b;
      nilSemSignal(&encoderButtonChange);
    }
    nilSemSignal(&encoderLock);
  }
}

NIL_WORKING_AREA(waConstantPowerThread, 96);
NIL_THREAD(constantPowerThread, arg) {
  while(1) {
    if(currentMode == MODE_CP) {
      nilThdSleepMilliseconds(50);
      nilSemWait(&targetLock);
      nilSemWait(&vSenseLoadLock);
      // targetPower * 25 / (vSenseLoad/500) = required current
      // current = 12500x/y
      targetPowerCurrent = (12500.0*targetPower)/vSenseLoad;
      nilSemSignal(&vSenseLoadLock);
      setDAC(targetPowerCurrent);
      nilSemSignal(&targetLock);
    } else {
      nilSemWait(&modeChangedToCP);
    }
  }
}

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  //if(nilIsIdleThread())
    delay(2); // Wait for Vref to settle
  //else
  //  nilThdSleepMilliseconds(2);
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1125300L / result; // Back-calculate AVcc in mV
  return result;
}

float readTemp() {
  unsigned int result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0);
  ADCSRA |= _BV(ADEN);  // enable the ADC
  ADCSRB |= _BV(MUX5);
  if(nilIsIdleThread())
    delay(5); // Wait for Vref to settle
  else
    nilThdSleepMilliseconds(5);
  ADCSRA |= _BV(ADSC); // Convert again, driver lag 2uS
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCW;
  return (result - calibration.temperatureOffset)/calibration.temperatureGain;
}

const uint8_t ADC_PS_BITS = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
const uint8_t ADC_PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);

enum { REF_VCC, REF_2V56 } referenceSource = REF_VCC;
float vccAverage = 0.0;
NIL_WORKING_AREA(waMonitoringThread, 96);
NIL_THREAD(monitoringThread, arg) {
  static systime_t startClock = 0;
  static unsigned char serialCounter = 0;
  while(1) {
    //nilSemWait(&monitoringTick);
    // Delay for a 20Hz cycle
    startClock = MS2ST(50)-startClock;
    if(startClock > MS2ST(50))
      startClock = MS2ST(50);
    nilThdSleep(startClock);
    startClock = nilTimeNow();
    
    analogReference(DEFAULT); analogRead(pin_vSense);
    vcc = readVcc();
    // Average = vccAverage * 0.8 + vcc * 0.2
    // Simply, Average = 0.2 * (4*vccAverage + vcc)
    vccAverage = 0.2 * (4*vccAverage + vcc);
    //vccAverage = vccAverage * 0.8 + vcc * 0.2;
    vcc = vccAverage;
    if(referenceSource == REF_2V56) {
      analogReference(INTERNAL); analogRead(pin_vSense);
      nilThdSleepMilliseconds(2);
    }
    
    uint16_t threshV = analogRead(pin_vSense);
    uint16_t threshI = analogRead(10);
    // Select a reference if the voltage range is low/high
    if(referenceSource == REF_VCC && threshV < 450 && threshI < 450) {
      analogReference(INTERNAL);
      referenceSource = REF_2V56;
      Serial.println("#REF2V56");
      analogRead(pin_vSense); // Discard one after reference changes
      nilThdSleepMilliseconds(2);
    } else if(referenceSource == REF_2V56 && (threshV > 950 || threshI > 950)) {
      analogReference(DEFAULT);
      referenceSource = REF_VCC;
      Serial.println("#REFVCC");
      analogRead(pin_vSense); // Discard one after reference changes
      nilThdSleepMilliseconds(2);
    }
    
    vSenseADC = 0;
    for(char i = 0; i < 16; i++) {
      vSenseADC += analogRead(pin_vSense);
    }
    vSenseADC /= 16.0;
 
    iSenseADC = 0;
    for(char i = 0; i < 16; i++) {
      iSenseADC += analogRead(10);
    }
    iSenseADC /= 16.0;
    
    // Vcc measured (mV) times calibration ratio (11.000 nominal), by the percentage of the ADC measured
    // Use the appropriate calibration value for the reference source
    if(referenceSource == REF_VCC) {
      vSenseLoadTmp = (vccAverage * calibration.vSenseMax) * (vSenseADC / 1023.0);
      iSenseLoadTmp = (vccAverage * calibration.iSenseMax) * (iSenseADC / 1023.0);
    } else if(referenceSource == REF_2V56) {
      // Internal reference is 1100 nominal
      // 2560 * calibration * (ADC/1023) ==
      // 2560/1023 * calibration * ADC ==
      // 2.50244379 * calibration * ADC
      //vSenseLoadTmp = 2560.0 * calibration.vSenseLow * (vSenseADC/1023.0);
      vSenseLoadTmp = 2.50244379 * calibration.vSenseLow * vSenseADC;
      iSenseLoadTmp = 2.50244379 * calibration.iSenseLow * iSenseADC;
    }
    
    nilSemWait(&currentCurrentLock);
    vSensePowerTmp = (vSenseLoad / 1000.0) * iSenseLoadTmp; //currentCurrent;
    nilSemSignal(&currentCurrentLock);
    
    nilSemWait(&vSenseLoadLock);
    vSenseLoad = vSenseLoadTmp;
    vSensePower = vSensePowerTmp;
    iSenseLoad = iSenseLoadTmp;
    nilSemSignal(&vSenseLoadLock);
    
    if(++serialCounter == 10) {
      NilSerial.print("c");
      NilSerial.println(vcc);
      NilSerial.print("R");
      NilSerial.println(referenceSource);
      NilSerial.print("V");
      NilSerial.println(vSenseLoad);
      NilSerial.print("I");
      NilSerial.println(currentCurrent);
      serialCounter = 0;
    }
    startClock = nilTimeNow() - startClock; // Total tick length
  }
}

NIL_WORKING_AREA(waDisplayThread, 96);
NIL_THREAD(displayThread, arg) {
  static char first = 9;
  modeTypes lastMode = MODE_OFF;
  uint16_t lastCurrent = 0, lastPower = 0,
           lastVSenseLoad = 0, lastVSensePower = 0,
           lastiSense = 0;
  displayModes lastDisplay = DISP_STANDARD;
  while(1) {
    nilThdSleepMilliseconds(100);
    
    if(lastDisplay != displayMode) {
      lcd.clear();
    }
    
    if(displayMode == DISP_POWER) {
      lcd.setCursor(0,0);
      lcd.print(F("MOS  "));
      // 2*(V-I)/1000*I/1000
      lcd.print((iSenseLoadTmp * (vSenseLoadTmp - iSenseLoadTmp)) / 500000.0);
      lcd.print("W");
      lcd.setCursor(0,1);
      lcd.print(F("PwrR "));
      // 2*(x^2/1000)
      lcd.print((iSenseLoadTmp*iSenseLoadTmp)/500000.0);
      lcd.print("W");
      lastDisplay = displayMode;
      continue;
    }
    
    // Display the current mode if it changed
    if(currentMode != lastMode || displayMode != lastDisplay) {
      lcd.setCursor(0, 0);
      if(currentMode == MODE_CC) {
        lcd.print(F("CC  "));
        lastCurrent = 65535;
      } else if(currentMode == MODE_CP) {
        lcd.print(F("CP  "));
        lastPower = 65535;
      } else if(currentMode == MODE_OFF) {
        lcd.print(F("Off         "));
      }
      lastMode = currentMode;
    }
    
    nilSemWait(&targetLock);
    if(lastDisplay != displayMode ||
       (currentMode == MODE_CC && (targetCurrent != lastCurrent || iSenseLoad != lastiSense))) {
      lcd.setCursor(3, 0);
      lcd.print(targetCurrent * 2);
      lcd.print(F("/"));
      lcd.print(iSenseLoad * 2);
      lcd.print(F("mA  "));
      lastCurrent = targetCurrent;
      lastiSense = iSenseLoad;
    } else if(currentMode == MODE_CP && targetPower != lastPower) {
      lcd.setCursor(4, 0);
      lcd.print(targetPower*25);
      lcd.print(F("mW    "));
      lastPower = targetPower;
    }
    nilSemSignal(&targetLock);
    
    if(lastDisplay != displayMode ||
       (vSenseLoad != lastVSenseLoad || vSensePower != lastVSensePower || ++first == 10)) {
      float v, w;
      first = 0;
      nilSemWait(&vSenseLoadLock);
      v = vSenseLoad / 1000.0;
      w = vSensePower / 500.0;
      lastVSenseLoad = vSenseLoad;
      lastVSensePower = vSensePower;
      nilSemSignal(&vSenseLoadLock);
      lcd.setCursor(0, 1);
      lcd.print(v, 2);
      lcd.print(F("V "));
      lcd.setCursor(6, 1);
      lcd.print(w, 1);
      lcd.print(F("W "));
    }
   
    lastDisplay = displayMode; 
  }
}

NIL_WORKING_AREA(waEncoderHandlerThread, 96);
NIL_THREAD(encoderHandlerThread, arg) {
  char buttonBeingHeld = 0;
  while(1) {
    if(nilSemWaitTimeout(&encoderChange,MS2ST(10)) == NIL_MSG_OK) {
      if(currentMode == MODE_CC) {
        nilSemWait(&targetLock);
        if(encoderValue > 4095) {
          encoderValue = targetCurrent = 4095;
        } else if(encoderValue > 0) {
          targetCurrent = encoderValue;
        } else {
          targetCurrent = 0;
        }
        float newTargetPoly = (calibration.polyC * targetCurrent * targetCurrent)
                              + calibration.polyB * targetCurrent
                              + calibration.polyA;
        if(newTargetPoly < 0.0)
          newTargetPoly = 0.0;
        setDAC(newTargetPoly); // targetCurrent);
        nilSemSignal(&targetLock);
      }
      if(currentMode == MODE_CP) {
        nilSemWait(&targetLock);
        if(encoderValue > 4095) {
          encoderValue = targetPower = 4095;
        } else if(encoderValue > 0) {
          targetPower = encoderValue;
        } else {
          targetPower = 0;
        }
        nilSemSignal(&targetLock);
      }
    }

    if(nilSemWaitTimeout(&encoderButtonChange, MS2ST(10)) == NIL_MSG_OK) {
      if(encoderButton == ClickEncoder::DoubleClicked) {
        currentMode = MODE_OFF;
      }
      if(encoderButton == ClickEncoder::Held && buttonBeingHeld == 0) {
        buttonBeingHeld = 1;
        if(displayMode == DISP_STANDARD)
          displayMode = DISP_POWER;
        else
          displayMode = DISP_STANDARD;
      }
      if(encoderButton == ClickEncoder::Released) {
        buttonBeingHeld = 0;
      }
      if(encoderButton == ClickEncoder::Clicked) {
        switch(currentMode) {
          case MODE_OFF:
            currentMode = MODE_CC;
            nilSemWait(&encoderLock);
            encoderValue = 0;
            nilSemSignal(&encoderLock);
            setDAC(0);
            break;
          case MODE_CC:
            currentMode = MODE_CP;
            nilSemWait(&encoderLock);
            nilSemWait(&targetLock);
            encoderValue = targetPower;
            nilSemSignal(&targetLock);
            nilSemSignal(&encoderLock);
            nilSemSignal(&modeChangedToCP);
            break;
          case MODE_CP:
            currentMode = MODE_CC;
            nilSemWait(&encoderLock);
            nilSemWait(&targetLock);
            encoderValue = targetCurrent;
            setDAC(targetCurrent);
            nilSemSignal(&targetLock);
            nilSemSignal(&encoderLock);
            break;
          default: currentMode = MODE_OFF; break;
        }
      }
    }
  }
}

void setDAC(uint16_t voltage) {
  digitalWrite(pin_dac_fs, HIGH);
  digitalWrite(pin_dac_ss, LOW);
  digitalWrite(pin_dac_fs, LOW);
  
  uint8_t highbyte;
  uint8_t lowbyte;
  
  // Highbyte bits 16-12 (8-5) are 0 for slow mode, normal operation
  highbyte = highByte(voltage & 4095);
  lowbyte  = lowByte(voltage & 4095);
  SPI.transfer(highbyte);
  SPI.transfer(lowbyte);
  
  // End transfer
  digitalWrite(pin_dac_ss, HIGH);
  
  if(!nilIsIdleThread()) {
    nilSemWait(&currentCurrentLock);
    currentCurrent = voltage;
    nilSemSignal(&currentCurrentLock);
  }
}

void setDACReference(char use2048 = 1) {
  digitalWrite(pin_dac_fs, HIGH);
  digitalWrite(pin_dac_ss, LOW);
  digitalWrite(pin_dac_fs, LOW);
  
  SPI.transfer(0b10010000);
  if(use2048)
    SPI.transfer(0b00000010); // 2.048V reference, internal
  else
    SPI.transfer(0b00000001); // 1.024V reference, internal
    
  digitalWrite(pin_dac_ss, HIGH);
  
  if(use2048)
    Serial.println(F("#REF2048"));
  else
    Serial.println(F("#REF1024"));
}

void setup() {
  Serial.begin(115200);
  Serial.println(F("#START"));
  
  NilSerial.begin(57600);
  
  //nilAnalogPrescalar(ADC_PS_128);
  
  pinMode(pin_dac_fs, OUTPUT);
  pinMode(pin_dac_ss, OUTPUT);
  
  pinMode(10, INPUT);
  
  // Retrieve calibrations
  eeprom_read_block((void *)&calibration, (void *)0, sizeof(calibration));
  if(calibration.calibrationVariant != CALIBRATION_VARIANT) {
    calibration.calibrationVariant = CALIBRATION_VARIANT;
    calibration.vSenseMax = 11.000; // External VCC reference
    calibration.vSenseLow = 11.000; // Internal 1.1V reference
    calibration.vSenseOffset = 0; // No 0V offset
    calibration.currentOffset = 0; // No current offset
    calibration.temperatureOffset = 271.5;
    calibration.temperatureGain = 1.06154;
    calibration.iSenseMax = 1.0;
    calibration.iSenseLow = 1.0;
    calibration.polyC = -1.869e-7;
    calibration.polyB = 1.015;
    calibration.polyA = -7.2187;
    eeprom_write_block((const void *)&calibration, (void *)0, sizeof(calibration));
  }
  
  encoder = new ClickEncoder(pin_rotary_a, pin_rotary_b, pin_rotary_e);
  
  // Setup for the TLV5636 DAC
  delay(100);
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE1);
  SPI.setClockDivider(SPI_CLOCK_DIV2);
  setDACReference();
  delay(1);
  setDAC(0);
  delay(1);
  setDAC(0); // For good measure
  
  lcd.begin(12,2);
  lcd.setCursor(0,0);
  lcd.print("DC Elec Load");
  delay(500);
  lcd.setCursor(0,1);
  lcd.print("  Nov 2014  ");
  
  delay(500);
  
  vcc = readVcc();
  lcd.setCursor(0,1);
  lcd.print("Vcc: ");
  lcd.print(vcc);
  lcd.print("mV ");
  
  delay(1250);
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("High: ");
  lcd.print(calibration.vSenseMax,3);
  lcd.setCursor(0,1);
  lcd.print("Low:  ");
  lcd.print(calibration.vSenseLow,3);
  
  delay(1250);
  
  lcd.clear();
  
  // WDT blows up Atmega32u4 bootloader
  //wdt_enable(WDTO_2S);
  //wdt_reset();
  
  // Start the RTOS
  nilSysBegin();
}

void loop() {
  // Reset watchdog here, since we need to be idling sometimes, or we're probably stuck
  //wdt_reset();
  if(Serial.available()) {
    char c = Serial.read();
    switch(c) {
      case 'o':
        currentMode = MODE_OFF;
        Serial.println(F("#OFF"));
        nilSemSignalI(&encoderChange);
        break;
      case 'c':
        currentMode = MODE_CC;
        Serial.println(F("#CC"));
        nilSemSignalI(&encoderChange);
        break;
      case 'p':
        currentMode = MODE_CP;
        Serial.println(F("#CP"));
        nilSemSignalI(&encoderChange);
        break;
      case 's':
        switch(Serial.read()) {
          case 'c':
            targetCurrent = Serial.parseInt()/2;
            encoderValue = targetCurrent;
            nilSemSignalI(&encoderChange);
            Serial.print(F("ISet "));
            Serial.println(targetCurrent);
            break;
          case 'p':
            targetPower = Serial.parseInt()/25;
            encoderValue = targetPower;
            nilSemSignalI(&encoderChange);
            Serial.print(F("PSet "));
            Serial.println(targetPower);
            break;
          default:
            Serial.println("Err");
            break;
        }
        break;
      case 'y':
        switch(Serial.read()) {
          case 'c':
            calibration.polyC = Serial.parseFloat();
            Serial.print(F("PolyC "));
            Serial.println(calibration.polyC,12);
            break;
          default:
            Serial.println("Err");
            break;
        }
        break;
      case 'r':
        Serial.print("M ");
        Serial.println(currentMode);
        Serial.print("c ");
        Serial.println(vcc);
        Serial.print("V ");
        Serial.println(vSenseLoad);
        Serial.print("I ");
        Serial.println(currentCurrent*2);
        Serial.print("r ");
        Serial.println(referenceSource);
        break;
      case 'C':
        Serial.print(F("#CAL.vSenseMax "));
        Serial.println(calibration.vSenseMax,4);
        Serial.print(F("#CAL.vSenseLow "));
        Serial.println(calibration.vSenseLow,4);
        Serial.print(F("#CAL.vSenseOffset "));
        Serial.println(calibration.vSenseOffset);
        Serial.print(F("#CAL.currentOffset "));
        Serial.println(calibration.currentOffset);
        Serial.print(F("#CAL.temperatureOffset "));
        Serial.println(calibration.temperatureOffset);
        Serial.print(F("#CAL.temperatureGain "));
        Serial.println(calibration.temperatureGain);
        Serial.print(F("#CAL.iSenseMax "));
        Serial.println(calibration.iSenseMax,4);
        Serial.print(F("#CAL.iSenseLow "));
        Serial.println(calibration.iSenseLow,4);
        Serial.print(F("#CAL.polyC "));
        Serial.println(calibration.polyC,4);
        Serial.print(F("#CAL.polyB "));
        Serial.println(calibration.polyB,4);
        Serial.print(F("#CAL.polyA "));
        Serial.println(calibration.polyA,4);
        break;
      case 'T':
        Serial.print(F("#Temp "));
        Serial.println(readTemp());
        break;
      case 'M':
        // Calibrate Max voltage sense
        // At 35V input level, calculate what the ratio should be
        calibration.vSenseMax = (35000.0/vcc) / (vSenseADC/1023.0);
        Serial.println(calibration.vSenseMax);        
        break;
      case 'L':
        // Calibrate Low voltage sense
        // At 12.5V input level, calculate what the ratio should be
        //Serial.println(vSenseLoad);
        //Serial.println(vSenseADC);
        // 2560 * X * (ADC / 1023) = 10000
        // 2560 * (ADC/1023) * X = 10000
        // (ADC/1023) * X = 10000/2560
        // (10000/2560)/(ADC/1023) = X
        calibration.vSenseLow = (12500.0/2560.0) / (vSenseADC/1023.0);
        Serial.println(calibration.vSenseLow);
        break;
      case 'E':
        // Calibrate current ratio on Vcc
        // Get it to 2000mA
        // Adjust offset
        calibration.iSenseMax = (2000.0/vcc) / (iSenseADC/1023.0) / 2.0;
        Serial.println(calibration.iSenseMax);
        break;
      case 'I':
        // Calibrate current ratio on Internal ref
        // Get it to 2000mA
        // Adjust offset
        calibration.iSenseLow = (2000.0/2560.0) / (iSenseADC/1023.0) / 2.0;
        Serial.println(calibration.iSenseLow);
        break;
      case 'O':
        // Calibrate current offset
        calibration.currentOffset = 2000 - iSenseLoad;
      case 'W':
        eeprom_write_block((const void *)&calibration, (void *)0, sizeof(calibration));
        Serial.println(F("Stored EEPROM"));
        break;
      default:
        break;
    }
  }
}

NIL_THREADS_TABLE_BEGIN()
NIL_THREADS_TABLE_ENTRY(NULL, rotaryEncoderThread, NULL, waRotaryEncoderThread, sizeof(waRotaryEncoderThread))
NIL_THREADS_TABLE_ENTRY(NULL, monitoringThread, NULL, waMonitoringThread, sizeof(waMonitoringThread))
NIL_THREADS_TABLE_ENTRY(NULL, constantPowerThread, NULL, waConstantPowerThread, sizeof(waConstantPowerThread))
NIL_THREADS_TABLE_ENTRY(NULL, encoderHandlerThread, NULL, waEncoderHandlerThread, sizeof(waEncoderHandlerThread))
NIL_THREADS_TABLE_ENTRY(NULL, displayThread, NULL, waDisplayThread, sizeof(waDisplayThread))
NIL_THREADS_TABLE_END()
